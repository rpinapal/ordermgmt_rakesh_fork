package com.freeLance.ordermanagement.serviceImpl;

import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.freeLance.ordermanagement.jpa.model.Order;
import com.freeLance.ordermanagement.jpa.repository.OrderRepo;
import com.freeLance.ordermanagement.service.OrderService;
import com.freeLance.ordermanagement.utility.RandomTrackerGenerator;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepo orderRepo;
	
	@Autowired
	private RandomTrackerGenerator trackerCode;
	
	@Override
	public Order createOrder(Order order) {
		if(order.getTrackerId() == null) {
			order.setTrackerId(trackerCode.generateTracker());
		} else if(order.getTrackerId().isEmpty()) {
			order.setTrackerId(trackerCode.generateTracker());
		}
		if(order.getCreationTime() == null)
			order.setCreationTime(new Timestamp(new Date().getTime()));
		return orderRepo.save(order);
	}

	@Override
	public boolean deleteOrder(Long id) {
		orderRepo.deleteById(id);
		return false;
	}

	@Override
	public Order getOrderByTrackingId(String trackerId) {
		return orderRepo.findByTrackerId(trackerId);
	}

	@Override
	public Order getOrderById(Long id) {
		return orderRepo.findById(id).get();
	}

	@Override
	public List<Order> getAllOrders() {
		return orderRepo.findAll();
	}

	@Override
	public Page<Order> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
//		Pageable pageable = PageRequest.of(1, 5);
//		Page<Order> orderPage = orderRepo.findAll(pageable);
//		return orderPage;
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
            Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		 return orderRepo.findAll(pageable);
		 
	}

}
