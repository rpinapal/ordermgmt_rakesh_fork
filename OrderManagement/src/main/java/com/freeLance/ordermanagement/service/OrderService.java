package com.freeLance.ordermanagement.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.freeLance.ordermanagement.jpa.model.Order;

public interface OrderService {
	public Order createOrder(Order order);
	public boolean deleteOrder(Long id);
	public Order getOrderByTrackingId(String trackerId);
	public Order getOrderById(Long id);
	public List<Order> getAllOrders();
	Page<Order> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
