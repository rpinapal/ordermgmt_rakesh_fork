package com.freeLance.ordermanagement.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.freeLance.ordermanagement.jpa.model.Order;

public interface OrderRepo extends JpaRepository<Order, Long>{
	public Order findByTrackerId(String trackerId);
}
